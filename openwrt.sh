#!/usr/bin/env bash

set -o errexit
set -o pipefail

function exit_usage() {
  echo "$(basename -- "$0") [--vbox-dir <path>] [--vbox-group <name>|--no-vbox-group] snapshot|<version>"
  exit 1
}

vbox_dir="$HOME/VirtualBox"
vbox_group=/Linux

while [ $# -gt 0 ] ; do
  case "$1" in
    --vbox-dir )
      [ -n "$2" ] || exit_usage
      vbox_dir="$(cd -- "$2" && pwd)"
      shift 2
      ;;
    --vbox-group )
      [ -n "$2" ] || exit_usage
      vbox_group="/$2"
      shift 2
      ;;
    --no-vbox-group )
      vbox_group=""
      shift 1
      ;;
    -* )
      exit_usage
      ;;
    * )
      break
      ;;
  esac
done

[ -n "$1" ] || exit_usage

case "$1" in
  snapshot | master )
    vm_name="OpenWrt Snapshot $(date +%s)"
    download_base_url=https://downloads.openwrt.org/snapshots/targets/x86/64
    image_gz_name=openwrt-x86-64-generic-squashfs-combined.img.gz
    ;;
  * )
    release_version="$1"
    vm_name="OpenWrt $release_version"
    download_base_url=https://downloads.openwrt.org/releases/"$release_version"/targets/x86/64
    case "$release_version" in
      17.* | \
      18.* | \
      19.* )
        image_gz_name=openwrt-"$release_version"-x86-64-combined-squashfs.img.gz
        ;;
      * )
        image_gz_name=openwrt-"$release_version"-x86-64-generic-squashfs-combined.img.gz
        ;;
    esac
    ;;
esac

image_name="${image_gz_name%.gz}"
vm_dir="$vbox_dir""$vbox_group"/"$vm_name"
dest_file="$vm_dir"/"${image_name%.img}".vdi
console_socket="$vm_dir"/console.sock

function cleanup() {
  [ -n "$temp_dir" ] \
    && [ "$temp_dir" != "/" ] \
    && rm -r -f "$temp_dir"
}

temp_dir="$(mktemp -d)"

trap cleanup EXIT

for file in "$image_gz_name" \
            sha256sums ; do
  curl --silent \
       --output "$temp_dir"/"$file" \
       "$download_base_url"/"$file"
done

shasum --algorithm 256 \
       "$temp_dir"/"$image_gz_name" \
  | awk '{ print $1 }' \
  | xargs -I {} \
          grep --quiet \
               --fixed-strings \
               --line-regexp \
               "{} *$image_gz_name" \
               "$temp_dir"/sha256sums

# Allow snapshots to fail with `trailing garbage ignored` error
if ! gunzip "$temp_dir"/"$image_gz_name" ; then
  [ $? -eq 2 ] && grep --quiet "Snapshot" <<< "$vm_name"
fi

dd if="$temp_dir"/"$image_name" \
   of="$temp_dir"/openwrt.img \
   bs=256m \
   conv=sync

[ ! -f "$dest_file" ] || exit 1

mkdir -p "$vm_dir"

VBoxManage convertfromraw \
           --format VDI \
           "$temp_dir"/openwrt.img \
           "$dest_file"

VBoxManage modifyhd \
           --resize 256 \
           "$dest_file"

VBoxManage createvm \
           --name "$vm_name" \
           --groups "$vbox_group" \
           --ostype Linux26_64 \
           --register \
           --basefolder "$vbox_dir"

VBoxManage storagectl \
           "$vm_name" \
           --name SATA \
           --add sata \
           --controller IntelAHCI \
           --portcount 1

VBoxManage storageattach \
           "$vm_name" \
           --storagectl SATA \
           --device 0 \
           --port 0 \
           --type hdd \
           --medium "$dest_file"

VBoxManage modifyvm \
           "$vm_name" \
           --memory 512 \
           --nic1 hostonly \
           --hostonlyadapter1 vboxnet0 \
           --nic2 nat \
           --uart1 0x3f8 4 \
           --uartmode1 server "$console_socket" \
           --audio none

VBoxManage startvm \
           --type headless \
           "$vm_name"

nc -U "$console_socket"
